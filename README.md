# README #

# How to Use It #

# Configure 
1. set Configure File 

		package main
		import (
			_ "bitbucket.org/peter1003/common/src/utils/config"
		)

		func main() {
			//Set Config Path
			// 請設定環境變數 CONFIG_PATH，預設為專案根目錄下的 config/config.yml
		}

# Validator
1. Creat Locale File

## Name Rule	

		{locale}.json

## Locale Format Sample 
	
		{
			"game": {
			"name": "Name",
			"id"	: "ID"
			}
		}
	
2. Apply to gin	

		package web
		import (
			validator "bitbucket.org/peter1003/common/src/utils/validator"
		)
		func main() {
			// 目錄是從當前執行的目錄算起
			validator.Boot("../../lang/validator", "en")
		}

3. Use Validator On Model/Controller

## Struct Sample 
	
		package struct
		type SlotRequest struct {
			Game    string `form:"game" validate:"required" comment:"game.name" `
			Wallet  string `form:"wallet" validate:"required"`
			GameId  string `form:"gameid" validate:"required"`
			Version string `form:"version" validate:"required"`
		}
## Controller
		package controller
		import (
			validator "bitbucket.org/peter1003/common/src/utils/validator"
		)
		func Test() {
			slotRequestBody := new(SlotRequest)
			//切换语系
			local := ctx.DefaultQuery("locale", "en")
			validator.ChangeLocale(local)
			if err := validator.Validate(ctx, slotRequestBody); err != nil {
				httputil.NewError(ctx, errors.Error(err, nil))
				return
			}
		}

#Redis Client Sample


	package controller
	import (
		 "bitbucket.org/peter1003/common/src/utils/redis"
	)
	func RedisTest() {
		defer cr.Close()
		redisKey := "redisKey"
		// resSetValue, err := redis.String(redisClient.Do("setex", redisKey, 600, 1))
		resSetValue, err := redis.String(cr.Do("setex", redisKey, 600, 1))
		if err != nil {
			msg := fmt.Sprintf("fail to set redis key=%s, error:%s", redisKey, err.Error())
			fmt.Println(msg)
		} else {
			msg := fmt.Sprintf("succeed to set redis key=%s, value=%#v", redisKey, resSetValue)
			fmt.Println(msg)
		}

		redisValue, err := redis.String(cr.Do("GET", redisKey))
		if err != nil {
			msg := fmt.Sprintf("fail to get content by redis key=%s, error:%s", redisKey, err.Error())
			fmt.Println(msg)
		} else {
			msg := fmt.Sprintf("succeed to get content by redis key=%s, value=%#v", redisKey, redisValue)
			fmt.Println(msg)
		}

		strRedisValue, err := redis.String(cr.Do("PING", nil))
		if err != nil {
			msg := fmt.Sprintf("fail to ping, error:%s", err.Error())
			fmt.Println(msg)
		} else {
			msg := fmt.Sprintf("succeed to ping, value=%#v", strRedisValue)
			fmt.Println(msg)
		}
	}

# Repository  Sample

	package controller
	import (
		model "bitbucket.org/Robert922/game_model/src/model"
		"bitbucket.org/peter1003/common/src/utils/repository"
		"strconv"
		"time"
	)
	func dbTest() {
		log := model.Agents{}
		queryResult := []model.Agents{}
		search := repository.Options{}
		// search["deleted"] = model.NOT_DELETED
		var options = map[string]interface{}{
			"search":  search,
			"page":    1,
			"perpage": 100,
			// "sort":    repo.GetSort(sort, log.SortMap()),
			"sort": "id desc",
		}
		repository.List(log.TableName(), &queryResult, options)
		t := time.Now()
		var inputValues = map[string]interface{}{
			"name":       "test" + t.String(),
			"is_enabled": 1,
		}
		repository.Save(log.TableName(), &log, inputValues)
		queryLog := model.Agents{}
		repository.Get(log.TableName(), &queryLog, strconv.FormatUint(log.ID, 10))
		repository.Delete(log.TableName(), &log)
}


# Use Locale 

## Configure Language Pack
 


	package main 
	import (
		i18n "bitbucket.org/peter1003/common/src/utils/i18n"
	)
	func main() {
		t := map[string]interface{}{
			"A": "a",
			"B": "b",
			"C": "c",
		}
		fmt.Println(i18n.L("BB", t))	
	}





	
