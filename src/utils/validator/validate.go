package validator

import (
	"bitbucket.org/peter1003/common/src/utils"
	"bitbucket.org/peter1003/common/src/utils/logger"
	"fmt"
	ut "github.com/go-playground/universal-translator"
	"gopkg.in/go-playground/validator.v9"
	"reflect"
	"regexp"
	"time"
)

func translateFunc(ut ut.Translator, fe validator.FieldError) string {
	t, err := ut.T(fe.Tag(), fe.Field())
	if err != nil {
		logger.ApplicationErrorSimple(fmt.Sprintf("validator translateFunc err, fe: %v", fe)+",err: %v", err)
		return fe.(error).Error()
	}
	return t
}

// 验证json
func ValidateJson(fl validator.FieldLevel) bool {
	// todo
	return true
}

func JsonTranslationsFunc(ut ut.Translator) (err error) {
	if err = ut.Add("json", "{0}不符合json格式", false); err != nil {
		logger.ApplicationErrorSimple("validator JsonTranslationsFunc err, err: %v", err)
	}
	return
}

// 验证phone
func ValidatePhone(fl validator.FieldLevel) bool {
	flag := false
	if ok, err := regexp.Match("^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\\d{8}$", getValueToByteArr(fl.Field())); nil != err {
		logger.ApplicationErrorSimple("validator ValidatePhone err, err: %v", err)
	} else if ok {
		flag = true
	}
	return flag
}

func PhoneTranslationsFunc(ut ut.Translator) (err error) {
	if err = ut.Add("phone", "{0}不符合手机格式", false); err != nil {
		logger.ApplicationErrorSimple("validator PhoneTranslationsFunc err, err: %v", err)
	}
	return
}

// 验证date
func ValidateDate(fl validator.FieldLevel) bool {
	flag := false
	if ok, err := regexp.Match("^\\d{4}(\\-|\\/|.)\\d{1,2}$1\\d{1,2}|\\d{6}$", getValueToByteArr(fl.Field())); nil != err {
		logger.ApplicationErrorSimple("validator ValidateDate err, err: %v", err)
	} else if ok {
		flag = true
	}
	return flag
}

func DateTranslationsFunc(ut ut.Translator) (err error) {
	if err = ut.Add("date", "{0}不符合日期格式", false); err != nil {
		logger.ApplicationErrorSimple("validator DateTranslationsFunc err, err: %v", err)
	}
	return
}

// 验证name
func ValidateName(fl validator.FieldLevel) bool {
	flag := false
	if ok, err := regexp.Match("^[\u4E00-\u9FA5A-Za-z0-9_]+$", getValueToByteArr(fl.Field())); nil != err {
		logger.ApplicationErrorSimple("validator ValidateName err, err: %v", err)
	} else if ok {
		flag = true
	}
	return flag
}

func NameTranslationsFunc(ut ut.Translator) (err error) {
	if err = ut.Add("name", "{0}不符合姓名格式", false); err != nil {
		logger.ApplicationErrorSimple("validator NameTranslationsFunc err, err: %v", err)
	}
	return
}

// 验证IdCard
func ValidateIdCard(fl validator.FieldLevel) bool {
	flag := false
	if ok, err := regexp.Match("^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$|^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|X|x)$", getValueToByteArr(fl.Field())); nil != err {
		logger.ApplicationErrorSimple("validator ValidateIdCard err, err: %v", err)
	} else if ok {
		flag = true
	}
	return flag
}

func IdCardTranslationsFunc(ut ut.Translator) (err error) {
	if err = ut.Add("idCard", "{0}不符合身份证格式", false); err != nil {
		logger.ApplicationErrorSimple("validator IdCardTranslationsFunc err, err: %v", err)
	}
	return
}

// 验证生日
func ValidateBirthDay(fl validator.FieldLevel) bool {

	// 满足日期的条件
	if !ValidateDate(fl) {
		return false
	}

	flag := false

	var val uint64
	switch fl.Field().Kind() {
	case reflect.Int, reflect.Int32, reflect.Int64:
		val = uint64(fl.Field().Int())
	case reflect.Uint, reflect.Uint32, reflect.Uint64:
		val = fl.Field().Uint()
	case reflect.String:
		val = utils.StrToUin64(fl.Field().String())
	}

	if val > 19000000 && val <= utils.StrToUin64(time.Now().Format("20060102")) {
		flag = true
	}
	return flag
}

func BirthDayTranslationsFunc(ut ut.Translator) (err error) {
	if err = ut.Add("birthday", "{0}不符合生日格式", false); err != nil {
		logger.ApplicationErrorSimple("validator BirthDayTranslationsFunc err, err: %v", err)
	}
	return
}

func getValueToByteArr(value reflect.Value) []byte {
	var byteArr []byte
	switch value.Kind() {
	case reflect.Int32:
		byteArr = utils.Int32ToByteArr(value.Interface().(int32))
	case reflect.Uint32:
		byteArr = utils.Uint32ToByteArr(value.Interface().(uint32))
	case reflect.Int64:
		byteArr = utils.Int64ToByteArr(value.Interface().(int64))
	case reflect.Uint64:
		byteArr = utils.Uint64ToByteArr(value.Interface().(uint64))
	case reflect.String:
		byteArr = []byte(value.String())
	}
	return byteArr
}
