package logger

import (
	"bitbucket.org/peter1003/common/src/utils/config"
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"path/filepath"
	"strings"
)

var applicationLog *zap.Logger
var businessLog *zap.Logger

func init() {
	hostName, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	filePath := config.GetString("logger.filePath")
	level := getZapLevel(config.GetString("logger.level"))
	maxSize := config.GetInt("logger.maxSize")
	maxBackups := config.GetInt("logger.maxBackups")
	maxAge := config.GetInt("logger.maxAge")
	compress := config.GetBool("logger.compress")
	serviceName := config.GetString("logger.serviceName")
	isShowConsole := config.GetBool("logger.isShowConsole")

	applicationFilePath := fmt.Sprintf("%vapplication/%v/%v_%v_log.log", filePath, serviceName, serviceName, hostName)

	businessFilePath := fmt.Sprintf("%vbusiness/%v/%v_%v_log.log", filePath, serviceName, serviceName, hostName)
	logFileExist(applicationFilePath)
	logFileExist(businessFilePath)
	applicationLog = newLogger(
		applicationFilePath,
		level,
		maxSize,
		maxBackups,
		maxAge,
		compress,
		serviceName,
		isShowConsole)

	businessLog = newLogger(
		businessFilePath,
		level,
		maxSize,
		maxBackups,
		maxAge,
		compress,
		serviceName,
		isShowConsole)
}

// make zepField
func Field(key string, val interface{}) zap.Field {
	return zap.Any(key, val)
}

// getZapLevel Mapping zap level
func getZapLevel(level string) zapcore.Level {
	switch l := strings.ToLower(level); l {
	case "debug":
		return zapcore.DebugLevel
	case "info":
		return zapcore.InfoLevel
	case "warn":
		return zapcore.WarnLevel
	case "error":
		return zapcore.ErrorLevel
	default:
		return zapcore.InfoLevel
	}
}

// ApplicationDebug add application debug log
func ApplicationDebug(msg string, fields ...zapcore.Field) {
	applicationLog.Debug(msg, fields...)
}

// ApplicationInfo add application info log
func ApplicationInfo(msg string, fields ...zapcore.Field) {
	applicationLog.Info(msg, fields...)
}

// ApplicationWarn add application warn log
func ApplicationWarn(msg string, fields ...zapcore.Field) {
	applicationLog.Warn(msg, fields...)
}

// ApplicationError add application error log
func ApplicationError(msg string, fields ...zapcore.Field) {
	applicationLog.Error(msg, fields...)
}

// ApplicationErrorSimple add application error log
func ApplicationErrorSimple(msg string, err error) {
	applicationLog.Error(msg, zap.Error(err))
}

// BusinessDebug add business debug log
func BusinessDebug(msg string, fields ...zapcore.Field) {
	businessLog.Debug(msg, fields...)
}

// BusinessInfo add business info log
func BusinessInfo(msg string, fields ...zapcore.Field) {
	businessLog.Info(msg, fields...)
}

//直接用 Application的
func Info(msg ...interface{}) {
	applicationLog.Info(fmt.Sprintf("%v", msg))
}

func logFileExist(file string) {
	dir := filepath.Dir(file)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		os.MkdirAll(dir, os.ModePerm)
	}
}
