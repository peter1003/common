package i18n

import (
	"fmt"
)

/**
 * 印出語系
 */
func L(key string, data ...map[string]interface{}) interface{} {
	//TODO ，先做假的，等之後再補上邏輯
	if len(data) > 0 {
		return key + parseData(data[0])
	} else {
		return key
	}
}

func parseData(data map[string]interface{}) string {
	str := ""
	for _, val := range data {
		str1 := fmt.Sprintf("%v", val)
		str += " " + str1
	}
	return str
}
