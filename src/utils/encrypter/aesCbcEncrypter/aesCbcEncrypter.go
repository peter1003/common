package aesCbcEncrypter

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"fmt"
	"io"
)

// pkcs7pad add pkcs7 padding
func pkcs7pad(data []byte, blockSize int) ([]byte, error) {
	if blockSize < 0 || blockSize > 256 {
		return nil, fmt.Errorf("pkcs7: Invalid block size %d", blockSize)
	} else {
		paddingLen := blockSize - len(data)%blockSize
		padding := bytes.Repeat([]byte{byte(paddingLen)}, paddingLen)
		return append(data, padding...), nil
	}
}

// pkcs7strip remove pkcs7 padding
func pkcs7strip(data []byte, blockSize int) ([]byte, error) {
	length := len(data)
	if length == 0 {
		return nil, errors.New("pkcs7: Data is empty")
	}
	if length%blockSize != 0 {
		return nil, errors.New("pkcs7: Data is not block-aligned")
	}
	paddingLen := int(data[length-1])
	ref := bytes.Repeat([]byte{byte(paddingLen)}, paddingLen)
	if paddingLen > blockSize || paddingLen == 0 || !bytes.HasSuffix(data, ref) {
		return nil, errors.New("pkcs7: Invalid padding")
	}
	return data[:length-paddingLen], nil
}

// Encrypt
func Encrypt(key, plainText []byte) (cipherText []byte, err error) {
	cipherText = make([]byte, 0)
	if len(plainText)%aes.BlockSize != 0 {
		paddedPlainText, padErr := pkcs7pad(plainText, aes.BlockSize)
		if padErr != nil {
			err = padErr
			return
		}
		plainText = paddedPlainText
	}

	block, newCipherErr := aes.NewCipher(key)
	if newCipherErr != nil {
		err = newCipherErr
		return
	}

	cipherText = make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, readErr := io.ReadFull(rand.Reader, iv); readErr != nil {
		err = readErr
		return
	}

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(cipherText[aes.BlockSize:], plainText)

	return
}

// Decrypt
func Decrypt(key, cipherText []byte) (plainText []byte, err error) {
	var block cipher.Block

	plainText = make([]byte, 0)
	if block, err = aes.NewCipher(key); err != nil {
		return
	}

	if len(cipherText) < aes.BlockSize {
		err = errors.New("ciphertext too short")
		return
	}
	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	if len(cipherText)%aes.BlockSize != 0 {
		err = errors.New("ciphertext is not a multiple of the block size")
		return
	}

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(cipherText, cipherText)

	plainText = cipherText

	return
}
