module bitbucket.org/peter1003/common

go 1.15

require (
	bitbucket.org/peter1003/slotorm v0.0.0-20200904091033-1cd2d0d95a04 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gomodule/redigo v1.8.2
	github.com/jinzhu/gorm v1.9.16
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.7.1
	go.uber.org/zap v1.10.0
	gopkg.in/go-playground/validator.v9 v9.31.0
)
