package repository

import (
	"bitbucket.org/peter1003/common/src/utils/config"
	"bitbucket.org/peter1003/common/src/utils/db"
	"bitbucket.org/peter1003/common/src/utils/redis"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

// Repository
func List(tableName string, rows interface{}, options Options) (int64, error) {
	search, _ := options["search"].(Options)
	page, _ := options["page"].(int64)
	perpage, _ := options["perpage"].(int64)
	if page == 0 {
		page = int64(1)
	}
	if perpage == 0 {
		perpage = int64(30)
	}
	sort, _ := options["sort"].(string)
	cond, vals, err := WhereBuild(search)

	if err != nil {
		return 0, err
	}

	var count int64
	//設定快取
	enableCache := config.GetBool("repository.enableCache")
	cacheKey := CacheKey(tableName, page, search, perpage, sort)
	countCacheKey := cacheKey + "|count"
	if enableCache {
		fmt.Println("enableCache")
		redisResultJsonString, _ := redis.GetBytes(cacheKey)
		countRedisString, _ := redis.Get(countCacheKey)
		if len(redisResultJsonString) == 0 || countRedisString == "" {
			if err := db.DB().Table(tableName).Where(cond, vals...).Order(sort).Offset(page * perpage).Scan(rows).Count(&count).Error; err != nil {
				return 0, err
			}
			jsonString, _ := json.Marshal(rows)
			ttl := config.GetInt("repository.ttl")
			writeToCache(cacheKey, jsonString, ttl)
			writeToCache(countCacheKey, strconv.FormatInt(int64(count), 10), ttl)
		} else {
			json.Unmarshal(redisResultJsonString, &rows)
			count, _ = strconv.ParseInt(countRedisString.(string), 10, 64)
		}
	} else {
		if err := db.DB().Table(tableName).Where(cond, vals...).Order(sort).Offset(page * perpage).Scan(rows).Count(&count).Error; err != nil {
			return count, err
		}
	}
	return count, nil
}

func Get(tableName string, rows interface{}, id string) error {
	//設定快取
	enableCache := config.GetBool("repository.enableCache")

	cacheKey := CacheKey(tableName, id)
	countCacheKey := cacheKey + "|1"
	if enableCache {
		redisResultJsonString, _ := redis.GetBytes(cacheKey)
		countRedisString, _ := redis.Get(countCacheKey)
		if len(redisResultJsonString) == 0 || countRedisString == "" {
			if err := db.DB().Table(tableName).Where("id = ?", id).Scan(rows).Error; err != nil {
				return err
			}
			jsonString, _ := json.Marshal(rows)
			writeToCache(cacheKey, jsonString, config.GetInt("repository.ttl"))
		} else {
			json.Unmarshal(redisResultJsonString, rows)
		}
	} else {
		if err := db.DB().Table(tableName).Where("id = ?", id).Scan(rows).Error; err != nil {
			return err
		}
	}
	return nil
}

func Save(tableName string, current interface{}, values Options) {
	tx := db.DB()
	if id, ok := values["id"]; ok {
		if id != "" {
			tx.Model(current).Updates(values).Where("id=?", id)
		} else {
			tx.FirstOrCreate(current)
		}
	} else {
		tx.Table(tableName).Save(current)
		tx.Model(current).Updates(values)
	}
	//刪除快取
	cacheKeyPerfix := CacheKey(tableName + "*")
	deleteCache(cacheKeyPerfix)
}

func Delete(tableName string, current interface{}) error {
	err := db.DB().Delete(current).Error
	//刪除快取
	cacheKeyPerfix := CacheKey(tableName + "*")
	deleteCache(cacheKeyPerfix)
	return err
}

func CacheKey(prefix string, vals ...interface{}) string {
	str := prefix
	if len(vals) > 0 {
		for _, v := range vals {
			str = str + "|" + fmt.Sprint(v)
		}
	}
	str = config.GetString("repository.cachePrefix") + str
	return str
}

//WhereBuild sql build where

func WhereBuild(where map[string]interface{}) (whereSQL string, vals []interface{}, err error) {
	for k, v := range where {
		ks := strings.Split(k, " ")
		if len(ks) > 2 {
			return "", nil, fmt.Errorf("Error in query condition: %s. ", k)
		}

		if whereSQL != "" {
			whereSQL += " AND "
		}
		strings.Join(ks, ",")
		switch len(ks) {
		case 1:
			//fmt.Println(reflect.TypeOf(v))
			switch v := v.(type) {
			case NullType:
				if v == IsNotNull {
					whereSQL += fmt.Sprint(k, " IS NOT NULL")
				} else {
					whereSQL += fmt.Sprint(k, " IS NULL")
				}
			default:
				whereSQL += fmt.Sprint(k, "=?")
				vals = append(vals, v)
			}
			break
		case 2:
			k = ks[0]
			switch ks[1] {
			case "=":
				whereSQL += fmt.Sprint(k, "=?")
				vals = append(vals, v)
				break
			case ">":
				whereSQL += fmt.Sprint(k, ">?")
				vals = append(vals, v)
				break
			case ">=":
				whereSQL += fmt.Sprint(k, ">=?")
				vals = append(vals, v)
				break
			case "<":
				whereSQL += fmt.Sprint(k, "<?")
				vals = append(vals, v)
				break
			case "<=":
				whereSQL += fmt.Sprint(k, "<=?")
				vals = append(vals, v)
				break
			case "!=":
				whereSQL += fmt.Sprint(k, "!=?")
				vals = append(vals, v)
				break
			case "<>":
				whereSQL += fmt.Sprint(k, "!=?")
				vals = append(vals, v)
				break
			case "in":
				whereSQL += fmt.Sprint(k, " in (?)")
				vals = append(vals, v)
				break
			case "like":
				whereSQL += fmt.Sprint(k, " like ?")
				vals = append(vals, v)
			}
			break
		}
	}
	return
}

func deleteCache(cacheKeyPerfix string) {
	enableCache := config.GetBool("repository.enableCache")
	if enableCache {
		//刪除快取
		redis.DelByPattern(cacheKeyPerfix)
	}
}
func writeToCache(cacheKey string, data interface{}, ttl int) {
	redis.SetEx(cacheKey, ttl, data)
}
