package db

import (
	"bitbucket.org/peter1003/common/src/utils/config"
	"bitbucket.org/peter1003/common/src/utils/logger"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"sync"
	"time"
)

var (
	lock     sync.Mutex
	db       *gorm.DB
	interval = config.GetInt("db.interval")
	dialect  = config.GetString("db.dialect")
	source   = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?%s",
		config.GetString("db.user"),
		config.GetString("db.password"),
		config.GetString("db.host"),
		config.GetString("db.port"),
		config.GetString("db.database"),
		config.GetString("db.flag"),
	)
)

func init() {
	go connectionPool()
}

// DB return database instance
func DB() *gorm.DB {
	if db != nil {
		return db
	}

	lock.Lock()
	defer lock.Unlock()

	if db != nil {
		return db
	}

	if db == nil {
		connect(dialect, source)
	}
	db.LogMode(config.GetBool("db.isShowConsole"))
	return db
}

// Close close database connection
func Close() {
	if db != nil {
		err := db.Close()
		if err != nil {
			logger.ApplicationErrorSimple("gorm close error", err)
		}
	}
}

func connect(dialect string, source string) {
	dbInstance, err := gorm.Open(dialect, source)

	if err != nil {
		logger.ApplicationErrorSimple("gorm connection open error", err)
	} else {
		dbInstance.SingularTable(true)
		dbInstance.BlockGlobalUpdate(true)
		dbInstance.DB().SetConnMaxLifetime(time.Duration(interval) * time.Second)
		dbInstance.DB().SetMaxIdleConns(200)
		dbInstance.DB().SetMaxOpenConns(200)
		db = dbInstance
	}
}

func connectionPool() {
	for {
		if err := DB().DB().Ping(); err != nil {
			logger.ApplicationErrorSimple("Error ping to DB", err)
			if db != nil {
				dbErr := db.Close()
				if dbErr != nil {
					logger.ApplicationErrorSimple("close db after ping db error", dbErr)
				}
			}
			connect(dialect, source)
		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
}
