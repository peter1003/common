package validator

import (
	"bitbucket.org/peter1003/common/src/utils/logger"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"sync"
	// "git.medlinker.com/golang/xerror"
	// "git.medlinker.com/medlinker/med-inquiry-common/library/logger"
	"github.com/gin-gonic/gin/binding"
	localEn "github.com/go-playground/locales/en"
	localZh "github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"gopkg.in/go-playground/validator.v9"
	translationsEn "gopkg.in/go-playground/validator.v9/translations/en"
	translationsZh "gopkg.in/go-playground/validator.v9/translations/zh"
)

//可变的Json物件
type varyLevel map[string]interface{}

var (
	uni                *ut.UniversalTranslator
	configValidateList = []struct {
		string
		validator.Func
		validator.RegisterTranslationsFunc
	}{
		{"json", ValidateJson, JsonTranslationsFunc},
		{"phone", ValidatePhone, PhoneTranslationsFunc},
		{"date", ValidateDate, DateTranslationsFunc},
		{"name", ValidateName, NameTranslationsFunc},
		{"idCard", ValidateIdCard, IdCardTranslationsFunc},
		{"birthday", ValidateBirthDay, BirthDayTranslationsFunc},
	}
	customFieldAry varyLevel
	currentLocale  string
)

type CustomValidator struct {
	once     sync.Once
	validate *validator.Validate
	trans    *ut.Translator
}

// 初始化gin验证器
// func init() {
// 	binding.Validator = new(CustomValidator)
// }
// 初始化gin验证器
func Boot(columnTransPath string, locale string) {
	customFieldAry = make(varyLevel)
	setCustomFieldTransPath(columnTransPath)
	currentLocale = locale
	binding.Validator = new(CustomValidator)
}

/**
 * 切换语系
 * @param locale string
 */
func ChangeLocale(locale string) {

	if _, exist := customFieldAry[locale]; exist {
		currentLocale = locale
		binding.Validator = new(CustomValidator)
	}

}

//設定客製化欄位路徑
func setCustomFieldTransPath(columnTransPath string) {
	_, file, _, _ := runtime.Caller(2)
	dir := filepath.Dir(file)
	// // get Test variable
	var configPath string

	configPath = dir + "/" + columnTransPath
	_, err := os.Stat(configPath)
	if !os.IsNotExist(err) {
		files, err := ioutil.ReadDir(configPath)
		if err != nil {
			logger.ApplicationErrorSimple("validator RegisterValidate err: %v", err)
		}
		for _, f := range files {
			loadJsonFile(configPath+"/"+f.Name(), fileNameWithoutExtension(f.Name()))
		}
	}
}

func fileNameWithoutExtension(fileName string) string {
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}

var _ binding.StructValidator = &CustomValidator{}

func (v *CustomValidator) ValidateStruct(obj interface{}) error {
	if kindOfData(obj) == reflect.Struct {
		v.lazyInit()
		if err := v.validate.Struct(obj); err != nil {
			errs := err.(validator.ValidationErrors)
			messages := make([]string, len(errs), len(errs))
			for i, e := range errs {
				messages[i] = e.Translate(*v.trans)
			}
			return errors.New(strings.Join(messages, ", "))
		}
	}
	return nil
}

func (v *CustomValidator) Engine() interface{} {
	v.lazyInit()
	return v.validate
}

func (v *CustomValidator) lazyInit() {
	v.once.Do(func() {
		// 国际化
		localZH := localZh.New()
		localEN := localEn.New()
		uni = ut.New(localZH, localEN)

		trans, _ := uni.GetTranslator(currentLocale)

		v.validate = validator.New()
		v.trans = &trans
		// validate.v9 tag 默认validate，兼容老代码
		v.validate.SetTagName("validate")
		switch currentLocale {
		case "en":
			translationsEn.RegisterDefaultTranslations(v.validate, trans)
		case "zh":
			translationsZh.RegisterDefaultTranslations(v.validate, trans)
		default:
			translationsZh.RegisterDefaultTranslations(v.validate, trans)
		}
		// 汉化验证提示
		// if err := translationsZh.RegisterDefaultTranslations(v.validate, trans); nil != err {
		// 	logger.ApplicationErrorSimple("validator lazyInit err: %v", err)
		// 	return
		// }

		// 自定义验证器 https://godoc.org/gopkg.in/go-playground/validator.v9
		for _, item := range configValidateList {
			if err := v.RegisterValidate(item.string, item.Func, trans, item.RegisterTranslationsFunc, translateFunc); nil != err {
				logger.ApplicationErrorSimple("validator RegisterValidate err: %v", err)
				return
			}
		}

		// 收集结构体中的comment标签，用于替换英文字段名称，这样返回错误就能展示中文字段名称了
		v.validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
			//透過 currentLocale 反查
			if fld.Tag.Get("comment") != "" {
				return column(fld.Tag.Get("comment"), customFieldAry[currentLocale].(varyLevel))
			} else {
				return fld.Tag.Get("comment")
			}
			// return fld.Tag.Get("comment")
		})
	})
}

// 注册Validate
func (v *CustomValidator) RegisterValidate(tag string, fn validator.Func, trans ut.Translator, registerFn validator.RegisterTranslationsFunc, translationFn validator.TranslationFunc) error {
	var err error
	if err = v.validate.RegisterValidation(tag, fn); nil != err {
		return err
	}
	if err = v.validate.RegisterTranslation(tag, trans, registerFn, translationFn); nil != err {
		return err
	}
	return err
}

func kindOfData(data interface{}) reflect.Kind {
	value := reflect.ValueOf(data)
	valueType := value.Kind()
	if valueType == reflect.Ptr {
		valueType = value.Elem().Kind()
	}
	return valueType
}

func Validate(ctx *gin.Context, targetStruct interface{}) error {
	if err := ctx.ShouldBind(targetStruct); err != nil {
		return err
	}
	return nil
}

//讀取JSON格式
func loadJsonFile(jsonFilePath string, locale string) {

	jsonFile, err := os.Open(jsonFilePath)
	if err != nil {
		logger.ApplicationErrorSimple("loadJsonFile: %v", err)
	}
	var t varyLevel
	byteValue, _ := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal(byteValue, &t)

	if err != nil {
		logger.ApplicationErrorSimple("loadJsonFile: %v", err)
	}
	customFieldAry[locale] = t
}

//根據 欄位取得對應的field ，命名格式相容laravel的 model.column
func column(fieldId string, locale map[string]interface{}) string {
	fields := strings.Split(fieldId, ".")

	if val, ok := locale[fields[0]]; ok {
		switch v := val.(type) {
		case string:
			return val.(string)
		default:
			return column(strings.Join(fields[1:], "."), v.(map[string]interface{}))
		}
	} else {
		return fieldId
	}
}
