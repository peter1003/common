package utils

import (
	"reflect"
	"time"
)

/*** Parese ***/

func StrToBool(str string) (b bool) {
	_ = ConvertAssign(&b, str)
	return
}

// bool to uint32
func BoolToUint32(b bool) (i uint32) {
	if b {
		return 1
	}
	return 0
}

// uint32 to bool
func Uint32ToBool(i uint32) (b bool) {
	_ = ConvertAssign(&b, i)
	return
}

// str to uint32
func StrToUin32(s string) (i uint32) {
	_ = ConvertAssign(&i, s)
	return
}

// uint32 to string
func Uint32ToStr(i uint32) (s string) {
	_ = ConvertAssign(&s, i)
	return
}

// str to uint64
func StrToUin64(s string) (i uint64) {
	_ = ConvertAssign(&i, s)
	return
}

// str to int64
func StrToInt64(s string) (i int64) {
	_ = ConvertAssign(&i, s)
	return
}

// uint64 to str
func Uint64ToStr(i uint64) (s string) {
	_ = ConvertAssign(&s, i)
	return
}

// int64 to str
func Int64ToStr(i int64) (s string) {
	_ = ConvertAssign(&s, i)
	return
}

// int to bool
func StrToInt(s string) (i int) {
	_ = ConvertAssign(&i, s)
	return
}

// int642timestamp
func Int642Totimestamp(i int64) string {
	return time.Unix(i, 0).Format("2006-01-02 15:04:05")
}

// s to m
func StructToMap(obj interface{}) map[string]interface{} {
	obj1 := reflect.TypeOf(obj)
	obj2 := reflect.ValueOf(obj)
	if reflect.Ptr == reflect.TypeOf(obj).Kind() {
		obj1 = obj1.Elem()
		obj2 = obj2.Elem()
	}
	var data = make(map[string]interface{})
	for i := 0; i < obj1.NumField(); i++ {
		data[obj1.Field(i).Tag.Get("json")] = obj2.Field(i).Interface()
	}
	return data
}

// int32 to []byte]
func Int32ToByteArr(i int32) (bytes []byte) {
	_ = ConvertAssign(&bytes, i)
	return
}

// uint32 to []byte]
func Uint32ToByteArr(i uint32) (bytes []byte) {
	_ = ConvertAssign(&bytes, i)
	return
}

// int64 to []byte]
func Int64ToByteArr(i int64) (bytes []byte) {
	_ = ConvertAssign(&bytes, i)
	return
}

// uint64 to []byte
func Uint64ToByteArr(i uint64) (bytes []byte) {
	_ = ConvertAssign(&bytes, i)
	return
}
