package config

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
	"regexp"
	// "runtime"
	"strings"
)

var deployConfigPath = "DEPLOY_PATH_REPLACE"

func init() {
	path := os.Getenv("CONFIG_PATH")
	if len(path) == 0 {
		path = "config/config.yml"
	}
	Load("config/config.yml")
}

// 路径为执行当下的目录开始算
func Load(path string) {
	// Viper will check for an environment variable any timeutil a viper.Get request is made.
	viper.AutomaticEnv()

	// get current file path
	// _, file, _, _ := runtime.Caller(1)
	// dir := filepath.Dir(file)
	dir, dirErr := os.Getwd()
	if dirErr != nil {
		fmt.Println(dirErr)
	}
	var configPath string

	configPath = dir + "/" + path
	// if test == "True" {
	// 	configPath = dir + "/../../../config/config.test.yml"
	// }

	// if deployConfigPath == "DEPLOY" {
	// 	configPath = "/var/www/html/systemAdminService/config/config.yml"
	// }

	err := Parse(configPath)
	if err != nil {
		panic(err)
	}
}

// Parse parse config file
func Parse(path string) error {
	configType := strings.Replace(filepath.Ext(path), ".", "", 1)
	viper.SetConfigType(strings.ToLower(configType))
	viper.SetConfigFile(path)
	viper.WatchConfig()

	err := viper.ReadInConfig()
	for _, key := range viper.AllKeys() {
		value := viper.GetString(key)
		envOrRaw := replaceEnvInConfig([]byte(value))
		viper.Set(key, string(envOrRaw))
	}
	return err
}

func replaceEnvInConfig(body []byte) []byte {
	search := regexp.MustCompile(`\$\{([^{}]+)\}`)
	replacedBody := search.ReplaceAllFunc(body, func(b []byte) []byte {
		group1 := search.ReplaceAllString(string(b), `$1`)

		envValue := os.Getenv(group1)
		if len(envValue) > 0 {
			return []byte(envValue)
		}
		return []byte("")
	})

	return replacedBody
}

// Get get config value by key
func Get(key string) interface{} {
	return viper.Get(key)
}

// Set set config
func Set(key string, value interface{}) {
	viper.Set(key, value)
}

// GetInt get config value of int by key
func GetInt(key string) int {
	return viper.GetInt(key)
}

// GetString get config value by key
func GetString(key string) string {
	return viper.GetString(key)
}

// GetBool get config value by key
func GetBool(key string) bool {
	return viper.GetBool(key)
}
