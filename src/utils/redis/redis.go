package redis

import (
	"bitbucket.org/peter1003/common/src/utils/config"
	"bitbucket.org/peter1003/common/src/utils/logger"
	"errors"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"io"
	"strings"
	"time"
)

type DbnoPool struct {
	redis.Pool
	dbno int
}

const (
	maxRetry = 3 //最多重試3次
)

var (
	RedisClientPool   *redis.Pool
	RedisConn         redis.Conn
	RConn             *redis.Conn
	currentRetryTimes int
)

func initRedis() {
	var (
		host string
		port string
		auth string
		db   int
	)

	host = config.GetString("redis.host")
	port = config.GetString("redis.port")
	auth = config.GetString("redis.auth") //redis connect密碼
	db = config.GetInt("redis.db")
	RedisClientPool = &redis.Pool{
		MaxIdle:     10,
		MaxActive:   20,
		IdleTimeout: 180,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", host+":"+port, redis.DialPassword(auth), redis.DialDatabase(db))

			if nil != err {
				return nil, err
			}
			return c, nil
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
}

func init() {
	initRedis()
}

func Close() {
	defer RedisConn.Close()
}
func Redis() *redis.Conn {
	if RConn == nil {
		RedisConn = RedisClientPool.Get()
		RConn = (&RedisConn)
	}
	return RConn
}

func ReConnect() error {
	if currentRetryTimes >= maxRetry {
		return errors.New("ReConnect Over")
	} else {
		//Sleep
		time.Sleep(1 * time.Second)
		RedisConn = RedisClientPool.Get()
		RConn = (&RedisConn)
		currentRetryTimes++
	}
	return nil
}

func ping(c redis.Conn) error {
	client := *Redis()
	_, err := redis.String(client.Do("PING"))
	if err != nil {
		return err
	}
	return nil
}

func GetBytes(key string) ([]byte, error) {
	var data []byte
	data, err := redis.Bytes(Do("GET", key))
	if err != nil {
		return data, fmt.Errorf("error getting key %s: %v", key, err)
	}
	return data, err
}

func Get(key string) (interface{}, error) {
	var data string
	data, err := redis.String(Do("GET", key))
	if err != nil {
		return data, fmt.Errorf("error getting key %s: %v", key, err)
	}
	return data, err
}

func DelByPattern(pattern string) error {

	keys, err := redis.Strings(Do("KEYS", pattern))

	if err != nil {
		// handle error
	}
	for _, key := range keys {
		_, err = Do("DEL", key)
		// r.Del(key)
	}
	return nil

}

func SetEx(key string, opt ...interface{}) (interface{}, error) {
	newOpt := []interface{}{
		key,
	}
	newOpt = append(newOpt, opt...)
	return Do("SETEX", newOpt...)
}

/**
 * Redis操作
 * @param string
 * @param {...[type]}
 * 請參考 https://godoc.org/github.com/gomodule/redigo/redis
 */
func Do(redisCmd string, opt ...interface{}) (output interface{}, err error) {
	if config.GetBool("redis.logCommand") {
		logger.Info("Redis Do Command" + redisCmd)
		logger.Info(opt...)
	}

	client := *Redis()
	if opt != nil {
		output, err = client.Do(redisCmd, opt...)
	} else {
		output, err = client.Do(redisCmd)
	}
	needNewConn := isConnError(err)
	if needNewConn {
		reconnectErr := ReConnect()
		if reconnectErr == nil {
			return Do(redisCmd, opt...)
		}
	}
	currentRetryTimes = 0
	return
}

func isConnError(err error) bool {
	var needNewConn bool

	if err == nil {
		return false
	}
	if err == io.EOF {
		needNewConn = true
	}
	if strings.Contains(err.Error(), "use of closed network connection") {
		needNewConn = true
	}
	if strings.Contains(err.Error(), "connect: connection refused") {
		needNewConn = true
	}
	if strings.Contains(err.Error(), "connection closed") {
		needNewConn = true
	}
	return needNewConn
}
